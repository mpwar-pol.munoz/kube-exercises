# Ingress Controller - HTTP

Los archivos necesarios para crear el objeto Ingress Controller junto al deployment y servicio correspondiente se encuentran en la carpeta `engingress`. Una vez dentro, podemos ejecutar los siguientes comandos:

```
kubectl create -f deployment.yaml
```
```
kubectl create -f service.yaml
```
```
kubectl create -f ingress.yaml
```

![Terminal screenshot](imgs/ingress_create.png "Terminal screenshot for the creation of the ingress object with all of its \"dependencies\"")

Podemos comprobar que se ha creado correctamente usando:

```
kubectl get ingresses
```

![Terminal screenshot](imgs/ingress_get.png "Terminal screenshot for the ingress's information")

Antes de poder comprobar su funcionamento deberemos añadir nuestra dirección (en este caso `pol.student.lasalle.com`) al archivo hosts de nuestro sistema operativo. 

Una forma fácil de hacerlo es formatando la IP de minikube para añadirla al archivo mediante vim:

```
echo "`minikube ip`\tpol.student.lasalle.com"
sudo vim /etc/hosts
```

![Terminal screenshot](imgs/ingress_hosts.png "Terminal screenshot for the edition of the hosts file")

Y finalmente verificamos su funcionamiento mediante el acceso por navegador:

![Browser screenshot](imgs/ingress_browser.png "Browser screenshot for ingress verification")

# Ingress Controller - HTTPS

Todos los archivos necesarios para realizar el mismo proceso que en el apartado anterior pero habilitando HTTPS se encuentran en la carpeta `secure_engingress`, aunque el deployment y el service son los mismos (por lo que hay que eliminarlos al inicio si ya existen).

Previamente a la creación de los objetos de Kubernetes, se creará un certificado TLS local mediante el siguiente comando:

```
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout tls/tls.key -out tls/tls.crt -subj "/CN=pol.student.lasalle.com"
```

**Nota:** Se ha usado el parámetro `-nodes` para evitar tener que introducir una passphrase.

El resultado se adjunta en la subcarpeta `secure_engine/tls`.

A continuación, habrá que crear un secret que contenga dicho certificado:

```
kubectl create secret tls engingress-tls-secret \
  --cert=tls/tls.crt \
  --key=tls/tls.key
```

![Terminal screenshot](imgs/secure_ingress_openssl.png "Terminal screenshot for the creation of the TLS certificate and corresponding ingress secret")

Y una vez creados los objetos (de la misma forma que en el apartado anterior, pero usando los archivos de la nueva carpeta) se puede intentar comprobar el resultado mediante navegador:

![Browser screenshot](imgs/secure_ingress_browser.png "Browser screenshot for HTTPS ingress verification")


Dado que el certificado es firmado por uno mismo, la mayoría de navegadores modernos no permiten el acceso a la página. Una alternativa puede ser obtenerlo por terminal:

```
curl --cacert tls/tls.crt https://pol.student.lasalle.com
```

![Terminal screenshot](imgs/secure_ingress_curl.png "Terminal screenshot for HTTPS ingress verification")