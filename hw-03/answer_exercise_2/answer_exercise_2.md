# StatefulSet

Antes de proceder a la realización del ejercicio, hay algunos aspectos que merece la pena comentar:

* Dados los requisitos del enunciado, no se ha configurado la persistencia de datos. Si se quisiera asegurar que a parte de existir el clúster de MongoDB sus datos se guardan disco para no perderlos en caso de caída, habría que usar Persistent Volumes.

* Algunos aspectos de las definiciones de objetos no tienen un objetivo immediatamente obvio:

    - El servicio declara `clusterIP: None` en su `spec` para ser headless. De esta forma kubernetes NO le asignará una IP interna automáticamente, permitiendo que los nodos de MongoDB interaccionen según su funcionamento interno.

    - El StatefulSet declara que los contenedores se levantarán ejecutando el comando `mongod --replSet replicaset --bind_ip 0.0.0.0`. De esta forma se levantará el daemon principal de MongoDB en cada nodo con los parámetros necesarios para que:

        -  Se pueda configurar la funcionalidad interna de replicación (`--replSet replicaset`)

        - Se permita la comunicacón con los demás nodos (`--bind_ip 0.0.0.0`)

## Proceso de configuración

El primer paso será crear el StatefulSet y su correspondiente Service:

```
kubectl create -f mongo_statefulset.yaml
```
```
kubectl create -f mongo_service.yaml
```

Una vez levantados, podemos entrar a la mongo Shell de cada uno de los pods mediante el siguiente comando (substituyendo el 0 por el número de pod al que queramos entrar):

```
kubectl exec -it mongo-statefulset-0 -- mongo
```

Para ver el estado de de la replicación nativa de MongoDB podemos usar la instrucción:

```
rs.status()
```

De forma similar, podemos obtener su configuración ejecutando:

```
rs.conf()
```

De todos modos, para que dicho proceso de replicación nativo de MongoDB funcione deberemos configurarlo en uno de los nodos, que actuará de master. En este caso podemos hacerlo como:

```
rs.initiate({
    _id: "replicaset", 
    version: 1, 
    members: [
        { _id: 0, host: "mongo-statefulset-0.mongo-service.default.svc.cluster.local:27017" },
        { _id: 1, host: "mongo-statefulset-1.mongo-service.default.svc.cluster.local:27017" },
        { _id: 2, host: "mongo-statefulset-2.mongo-service.default.svc.cluster.local:27017" }
    ]
})
```

## Prueba de funcionamiento

Para ver que se ha configurado correctamente el clúster con replicación de MongoDB se realizará el siguiente proceso:


1. Se usará la database `lasalle` en todos los nodos, empezando por el 0 que la creará si es necesario.
```
use lasalle
```

2. Se pedirá permiso para ejecutar operaciones de lectura en los nodos 1 y 2 (secundarios).
```
rs.secondaryOk()
```

3. Se verificará que la tabla `students` no existe o está vacía en todos los nodos.
```
db.students.find()
```

4. En el nodo 0 se introducirá un valor en dicha tabla:
```
db.students.insert({ name: "Pol", studies: "MPWAR" })
```

5. Se comprobará que todos los nodos hayan visto la inserción, ejecutando de nuevo:
```
db.students.find()
```

Todo lo explicado hasta este punto puede observarse en el siguiente video:

![Terminal recording](imgs/mongo.mov)

# Diferencias con ReplicaSet

Un ReplicaSet trata a sus Pods como partes intercambiables del sistema que pueden ser eliminadas en cualquier momento, sustituyéndolas por otras equivalentes pero con identificadores distintos. Un StatefulSet en cambio trata cada Pod de forma secuencial, persistiendo su identificador a través de reinicios.

Así, si hubiesemos usado un ReplicaSet en este ejercicio la replicación interna de los nodos MongoDB se tendría que reconfigurar cada vez que se recrease un Pod, ya que su identificador habría cambiado.

Además, el orden en que un StatefulSet crea/elimina sus Pods siempre es el mismo, también secuencialmente, por lo que si asignamos el Pod 0 como maestro en el sistema de MongoDB sabemos que será el último en caer.