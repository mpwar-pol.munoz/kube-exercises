# Horizontal Pod Autoscaler

En este ejercicio se ha usado el mismo deployment y service que en el primero, pero reduciendo los límites de CPU declarados para que el autoescalado ocurra antes. Los comandos usados son:

```
kubectl create -f deployment.yaml
```
```
kubectl create -f service.yaml
```

A continuación, se ha aplicado un Horizontal Pod Autoscaler al deployment usando el archivo correspondiente.

```
kubectl create -f hpa.yaml
```

**Nota:** El comportamiento requerido de "escalar al doble de réplicas" dependerá del uso concreto de la cpu cuando el algoritmo interno del autoescalador calcule el nuevo número de Pods.


Como se sugiere en el enunciado, se puede comprobar el funcionamiento del autoescalador mediante una prueba de estrés:

```
kubectl run -i --tty load-generator --rm --image=busybox --restart=Never -- /bin/sh -c "while sleep 0.01; do wget -q -O- http://enginequis-service; done"
```

## Demostración

El comportamiento del autoescalador puede observarse en el siguiente video:

[![Terminal recording](http://img.youtube.com/vi/3YptiSGvIg4/0.jpg)](http://www.youtube.com/watch?v=3YptiSGvIg4 "Terminal recording for the horizontal pod autoscaler behaviour")


**Nota:** Se ha subido a YouTube por limitaciones de tamaño y para dividirlo en secciones que expliquen lo que está pasando.