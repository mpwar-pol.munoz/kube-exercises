# kube-exercises

Repository for miscellaneous Kubernetes exercises. MPWAR - La Salle URL. 2020-2021 Academic Year.

**Student:** Pol Muñoz Pastor (pol.munoz)

## hw-02

This folder includes solutions for the second deliverable.

## hw-03

This folder includes solutions for the third deliverable.

## hw-05

This folder includes solutions for the fifth deliverable.
