# Deployment

Para comprobar el funcionamento de este ejercicio se usará el servicio 3 del ejercicio anterior, juntamente con los siguientes comandos:

- Observación en vivo del estado de los pods:
```
kubectl get pods -w
```

- Observación en vivo del estado del deployment:
```
kubectl get deployments -w
```

- Comprovación del estado y la versión de la aplicación:

```
while true; do curl -s 192.168.99.100:32323/test.html | grep -o -E 'nginx\/\d+.\d+.\d+'; [[ $? -eq 0 ]] || echo "KO"; sleep .5; done
```

**Nota:** 192.168.99.100 es la IP de minikube. 32323 el puerto específico del servicio. En el siguiente ejercicio se exponen como variables de entorno.


**Explicación**

Para comprobar el estado y versión de la aplicación se obtiene periódicamente una página de error 404 a propósito (`test.html` no existe), que devuelve lo siguiente:

![Browser screenshot](imgs/nginx_404.png "Browser screenshot for a 404 error page")

A continaución se filtra la versión de nginx mediante grep y una expresión regular. 

Si grep no encuentra ningún match (cuando curl no devuelve "nada") termina la ejecución con código 1, lo que se usa como un indicador de degradación de servicio, mostrando el mensaje "KO" para apreciarlo visualmente.

La actualización consistirá en migrar de la versión 1.19.4 de nginx a la 1.19.6, por lo que se verá el cambio fácilmente.

## Recreate

Se creará este deployment usando:

```
kubectl create -f recreate.yaml
```

Para actualizar la versión de este deployment se usará el comando:

```
kubectl set image deployment/enginequis-recreate enginequis=nginx:1.19.6-alpine --record
```

Juntamente con el siguiente para volver a la versión anterior (apartado 3):

```
kubectl rollout undo deployment/enginequis-recreate
```

![Terminal recording](imgs/recreate_update_rollback.mov)

## Rolling Update

Como observación, se ha escogido los parámetros `maxUnavailable=1` y `maxSurge=2`teniendo en cuenta que el número de réplicas elegidas eran `3`.

Se creará este deployment usando:

```
kubectl create -f rolling.yaml
```

Para actualizar la versión de este deployment se usará el comando:

```
kubectl set image deployment/enginequis-rolling enginequis=nginx:1.19.6-alpine --record
```

Juntamente con el siguiente para volver a la versión anterior (apartado 3):

```
kubectl rollout undo deployment/enginequis-rolling
```

![Terminal recording](imgs/rolling_update_rollback.mov)

La principal ventaja de esta estrategia frente recreate es que no hay degradación de servicio por actualización.

A pesar de que no se aprecia en el video, un pequeño inconveniente del rolling update es que un usuario podría encontrarse con versiones alternantes del servicio según el momento en que haga las peticiones.

## Rollback

En los apartados anteriores se ha comentado y demostrado el uso del comando `kubectl rollout undo` para rollbacks. 

Como complemento, se puede usar el flag `--revision` para escoger la revisión a la que volver, consultables mediante el comando:

```
kubectl rollout history deployment/enginequis-rolling
```
