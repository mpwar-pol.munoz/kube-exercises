# Blue/Green Deployment

En este ejercicio se pide ejecutar un deployment Blue/Green, que comparte los beneficios con Rolling Update pero además ofrece al equipo de desarrollo la posibilidad de "testear" en producción sin afectar a los usuarios, que acaban viendo un cambio instantáneo.

La principal desventaja de esta estrategia es que se usan más recursos, al emplazar el deployment de la nueva versión en paralelo a la vieja.


En esta ocasión se crearán dos deployments: `deploymentBlue.yaml` que representará ser la versión actual de la aplicación (nginx 1.19.4) y `deploymentGreen.yaml`que será la nueva.

De forma similar, se usarán dos servicios: `serviceBlue.yaml` al que atacarán los usuarios en todo momento y `serviceGreen.yaml`, que servirá para testear en producción. Para poder ejecutar el escenario con minikube se les han asignado los NodePorts `31111` y `32222` respectivamente.

Si se quisiera dejar que kubernetes asignase por defecto los puertos sería recomendable exportarlos como variables de entorno, una vez creados:

```
export BLUE_PORT=$(kubectl get service enginequis-blue -o 'jsonpath={.spec.ports[0].nodePort}')
```

```
export GREEN_PORT=$(kubectl get service enginequis-green -o 'jsonpath={.spec.ports[0].nodePort}')
```

En esta ocasión se ha optado por simplificar el proceso y exportarlos de forma manual, juntamente a la IP de minikube:

```
export BLUE_PORT=31111
```
```
export GREEN_PORT=32222
```
```
export NODE_IP=$(minikube ip)
```

Una vez declaradas las tres variables de entorno, el proceso paso a paso será el siguiente:

0. Observar los deployments y servicios en vivo con los siguientes comandos:

```
kubectl get deployments -w
```

```
kubectl get services -w
```

1. Levantar el deploymentBlue y serviceBlue, correspondientes a la versión actual.

```
kubectl create -f deploymentBlue.yaml
```
```
kubectl create -f serviceBlue.yaml
```

2. Simular el tráfico de los usuarios, obteniendo la versión de la aplicación como se ha descrito en el ejercicio anterior:

```
echo "We are users"; while true; do curl -s $NODE_IP:$BLUE_PORT/test.html | grep -o -E 'nginx\/\d+.\d+.\d+'; [[ $? -eq 0 ]] || echo "Users KO"; sleep .5; done
```

3. Levantar el deploymentGreen y serviceGreen, correspondientes a la versión nueva.

```
kubectl create -f deploymentGreen.yaml
```
```
kubectl create -f serviceGreen.yaml
```

4. Simular el tráfico del equipo de testeo, de forma similar al de los usuarios:

```
echo "We are testers"; while true; do curl -s $NODE_IP:$GREEN_PORT/test.html | grep -o -E 'nginx\/\d+.\d+.\d+'; [[ $? -eq 0 ]] || echo "Testers KO"; sleep .5; done
```

5. En cuanto se desee mover a los usuarios a la nueva versión, actualizar el serviceBlue a seleccionar los pods que tengan la etiqueta correspondiente:

```
kubectl patch service enginequis-blue -p '{"spec":{"selector":{"app": "nginx-server-v2"}}}'
```

6. Finalmente, eliminar el serviceGreen (los testers ya no necesitarán acceso) y el deploymentBlue (la versión antigua):

```
kubectl delete service enginequis-green
```
```
kubectl delete deployment enginequis-v1
```

![Terminal recording](imgs/bluegreen.mov)

A pesar de que no se observa bien al final del video, después de ejecutar el proceso solo queda un deployment:

![Terminal screenshot](imgs/after.png "Terminal screenshot for the bluee/green deployment aftermath")

Una desventaja a tener en cuenta de esta estrategia es que al no ser por defecto de Kubernetes no tendremos acceso a las herramientas de versionado ni de rollback (especialmente al estar cambiando de deployment a cada update). 