# Service

Para este ejercicio se han escogido distintos tipos de servicio según los requerimientos de cada apartado. En concreto, se han definido:

- **LoadBalancer** para exponer el servicio al exterior mediante una IP pública.

- **ClusterIP** para exponer el servicio únicamente de forma interna.

- **NodePort** Para abrir un puerto específico de la VM (concretamente el 32323).

Todos los servicios se pueden levantar usando el comando:

```
pod create -f servicioX.yaml
```

Donde X es el número de servicio a crear (1, 2 ó 3).

Si bien todos se pueden levantar de esta forma, LoadBalancer no funcionará correctamente en un entorno minikube (ya que no podrá obtener una IP pública). Por ello la comprobación del funcionamiento se ha hecho con el servicio NodePort.

![Terminal screenshot](imgs/services.png "Terminal screenshot for the creation and testing of the exercise's services")

**Nota:** 192.168.99.100 es la IP de minikube. 32323 el puerto específico del servicio 3.