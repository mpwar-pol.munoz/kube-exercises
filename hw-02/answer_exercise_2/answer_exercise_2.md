# ReplicaSet

Podemos usar la definición del pod anterior al declarar un ReplicaSet. Para crearlo solo deberemos ejecutar el comando:

```
kubectl create -f replica_enginequis.yaml
```

Podemos apreciar la gestión de sus pods si "observamos" el estado de los pods en vivo:

```
kubectl get pods -w
```

Y así comprobamos que si eliminamos uno se volverá a crear: 

```
kubectl delete pod enginequis-<hashId>
```

![Terminal screenshot](imgs/replicaset.png "Terminal screenshot for the replicaset creation and testing")

**Nota:** En el caso de la captura se eliminó el pod `enginequis-cjtf7`, lo cual llevó a que el ReplicaSet crease el `enginequis-tkzvf` automáticamente.

## Pregunta 1: Escalando a 10

Para escalar el ReplicaSet a 10 pods en vivo se puede usar el siguiente comando:

```
kubectl scale replicaset enginequis --replicas=10
```

![Terminal screenshot](imgs/replicaset_scale.png "Terminal screenshot for the replicaset scaling")

Si "observamos" de nuevo los pods, y en esta ocasión también los replicasets, veremos cómo evolucionan a medida que se realiza el escalado:

```
kubectl get pods -w
```

![Terminal screenshot](imgs/replicaset_scale_pods.png "Terminal screenshot for the replicaset scaling seen in pods")

```
kubectl get replicasets -w
```

![Terminal screenshot](imgs/replicaset_scale_replicasets.png "Terminal screenshot for the replicaset scaling seen in replicasets")

**Nota:** En esta pregunta los pods del principio son diferentes a los de las capturas anteriores porque paré la VM de minikube para alocarle más recursos. Por defecto tiene 2GB de RAM y no podía alocar 10 veces 256Mi, así que 3 pods se quedaban en "Pending".

![Terminal screenshot](imgs/replicaset_scale_resources.png "Terminal screenshot for the replicaset scaling without resources")

## Pregunta 2: Asegurar una replica por nodo

Para asegurar que cada pod va a estar por lo menos una vez en cada nodo de kubernetes hay que usar el objeto DaemonSet en vez de ReplicaSet.