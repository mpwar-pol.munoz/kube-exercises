# nginx Pod

Una vez definimos el pod de forma declarativa mediante el archivo `enginequis.yaml` podemos crearlo ejecutando el comando:

```
kubectl create -f enginequis.yaml
```

![Terminal screenshot](imgs/pod_create.png "Terminal screenshot for the creation of the exercise's pod")

## Pregunta 1: Logs

Para obtener las últimas 10 líneas de salida estándar se puede usar el comando:

```
kubectl logs enginequis-v1 --tail=10
```

![Terminal screenshot](imgs/pod_logs.png "Terminal screenshot for the collection of pod's logs")

También puede ser interesante "seguir" los logs del pod mediante el flag `-f`, que bloqueará el terminal y mostrará los mensajes a medida que aparezcan.

```
kubectl logs -f enginequis-v1 --tail=10
```

## Pregunta 2: IP

Para obtener la IP interna del pod podemos mostrar su información extendida mediante el siguiente comando:

```
kubectl get pods -o wide
```

![Terminal screenshot](imgs/pod_wide.png "Terminal screenshot for the pod's information (wide output)")

La IP interna del pod se puede ver en la sexta columna, aunque si se quiere exportar como variable de entorno se puede ejecutar el siguiente comando:

```
export POD_IP=$(kubectl get pods enginequis-v1 -o 'jsonpath={.status.podIP}')
```

![Terminal screenshot](imgs/pod_ip.png "Terminal screenshot for the export of the pod's IP")

Que obtiene la información del pod en concreto en formato JSON, accediendo a un solo atributo.

## Pregunta 3: Entrar al pod

Podemos entrar al único container del pod ejecutando el comando:

```
kubectl exec -it enginequis-v1 -- /bin/sh
```

Que nos logueará a una terminal del container, donde podremos ejecutar los comandos que queramos:

![Terminal screenshot](imgs/pod_exec.png "Terminal screenshot for entering the pod's container's terminal")

## Pregunta 4: Visualizar el contenido

Para ver el contenido expuesto por nginx, podemos usar el comando:


```
kubectl port-forward enginequis-v1 8080:80
```

![Terminal screenshot](imgs/pod_forward.png "Terminal screenshot for forwarding a pod's port")

### Usando servicios

Como alternativa, se puede crear un servicio mediante el siguiente comando:

```
kubectl expose pod enginequis-v1 --type=NodePort --port=80
```

Lo cual nos permite acceder al contenido servido por nginx si sabemos la IP y puerto del nodo al que queremos conectarnos (en este caso minikube).

![Terminal screenshot](imgs/pod_curl.png "Terminal screenshot for getting the pod's content")

De nuevo, podemos exportar la IP y puerto como variables de entorno:

![Terminal screenshot](imgs/pod_export_curl.png "Terminal screenshot for exporting the node's information and getting the pod's content")

## Pregunta 5: QoS

Podemos comprobar la calidad de servicio que kubernetes ha asignado a nuestro pod (Guaranteed en este caso) si obtenemos toda su información (formato yaml o json):

```
kubectl get pods enginequis-v1 -o yaml
```

```
kubectl get pods enginequis-v1 -o json
```

Alternativamente, también podemos acceder al atributo que nos interesa (status > qosClass):

```
kubectl get pods enginequis-v1 -o 'jsonpath={.status.qosClass}'
```

![Terminal screenshot](imgs/pod_qos.png "Terminal screenshot for the pod's QoS class")

Finalmente, dicha información también se puede conseguir describiendo el pod:

```
kubectl describe pod enginequis-v1
```
